import hashlib
import json
from time import time
from textwrap import dedent
from uuid import uuid4
from urllib.parse import urlparse
import requests
import energyusage

from flask import Flask




class Blockchain(object):
    def __init__(self):
        self.chain = []
        self.current_transactions = []
        self.new_block(previous_hash=1, proof=100) # genesis block creation
        self.nodes = set()

    def register_node(self, address):
        # function to add node to node list
        parsed_url = urlparse(address)
        self.nodes.add(parsed_url.netloc)

    def new_block(self, proof, previous_hash=None):
        # create a new block on the chain
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transactions': self.current_transactions,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]) # either provide prev_hash or grab from prev block
        }

        # reset list of transactions for new block
        self.current_transactions = []

        self.chain.append(block)
        return block
    
    def new_transaction(self, sender, receiver, amount):
        # create a new transaction to transaction list
        self.current_transactions.append({
            'sender': sender, # sender's address in a str
            'receiver': receiver, # receiver's address in a str
            'amount': amount, # amount sent from sender to receiver
        })

        return self.last_block['index'] + 1 # index of the block that stores this transaction

    def proof_of_work(self, last_proof):
        # find a number p' where hash(pp') has 4 leading zeros, where p is prev proof
        start = time()
        proof = 0
        while self.valid_proof(last_proof, proof) is False:
            proof += 1
        print("Run time of PoW: " + str(round(time() - start)) + " seconds.")
        return proof

    def valid_chain(self, chain):
        # used for consensus, determines validity
        lastblock = chain[0]
        index = 1

        while index < len(chain):
            block = chain[index]
            print(f'{lastblock}')
            print(f'{block}')
            print("\n-----------\n")

            if block['previous_hash'] != self.hash(lastblock):
                return False  # check block for correct hash

            if not self.valid_proof(lastblock['proof'], block['proof']):
                return False  # check for correct PoW

            last_block = block
            index += 1

        return True

    def resolve_conflict(self):
        # consensus algorithm, replace chain with longest on network
        neighbors = self.nodes
        new_chain = None

        maxlen = len(self.chain)

        for node in neighbors:  # gets all nodes on network
            response = requests.get(f'http://{node}/chain')

            if response.status_code == 200:
                length = response.json()['length']
                chain = response.json()['chain']

                if length > maxlen and self.valid_chain(chain):
                    maxlen = length
                    new_chain = chain

        if new_chain:  # if we found a valid new chain, replace chain
            self.chain = new_chain
            return True

        return False

    @staticmethod
    def valid_proof(last_proof, proof):
        # valdiates proof, does hash(lastproof, proof) contain 4 lead zero?

        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha512(guess).hexdigest()
        return guess_hash[:6] == "000000"

    @staticmethod
    def hash(block):
        # SHA-256 hash
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha512(block_string).hexdigest()


    @property
    def last_block(self):
        # return last block in chain
        return self.chain[-1]



