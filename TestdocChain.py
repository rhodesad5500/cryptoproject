from docChain import getNewChain
import docChain

from flask import Flask, render_template, request
from hashlib import md5
from datetime import datetime

app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def upload():
    if request.method == "POST":
        if request.form['inputType'] == "message":
            print("got fileType = message = " + request.form['inputType'])
            app.config["docChain"].addMessage(request.form['message'])
        elif request.form['inputType'] == "File":
            file = request.files['file']
            #print(file.read())
            print(type(file))
            print(file)
            print(id(file))
            print(dir(file))
            print(file.content_length)
            #print(file.read())
            print("got fileType = file")
            app.config["docChain"].addFile(file)
    return render_template("docChain.html")


@app.route("/docChain")
def showChain():
    blocks = app.config['docChain'].blocks
    return render_template("viewDocChain.html", blocks=blocks)


@app.route("/docChain/view/<int:index>")
def showBlock(index):
    block = app.config['docChain'].blocks[index]
    currentHash = ""
    if block.docType == "file":
        file = open(block.docRef)
        currentHash = md5(open(block.docRef, 'rb').read()).hexdigest()
    else:
        currentHash = md5(block.docRef.encode())
    return render_template("viewDoc.html", block=block, currentHash=currentHash)


@app.template_filter('formatTime')
def getFormattedTime(s):
    return datetime.fromtimestamp(s)


if __name__ == '__main__':
    chain = docChain.getNewChain()
    app.config["docChain"] = chain
    app.config['UPLOAD_FOLDER'] = "/docChainFiles"
    app.run(debug=True)
