import sys
import energyusage
from flask import Flask, request
from flask import jsonify
from blockchain import Blockchain
from textwrap import dedent
from time import time
from uuid import uuid4
import requests
import views

# Instantiate our Node
app = Flask(__name__)

# Generate a globally unique address for this node
node_identifier = str(uuid4()).replace('-', '')

# Instantiate the Blockchain
blockchain = Blockchain()


@app.route('/mine', methods=['GET'])
def mine():
    # run PoW algorithm for next proof
    last_block = blockchain.last_block
    last_proof = last_block['proof']
    proof = blockchain.proof_of_work(last_proof)
    # energyusage.evaluate(blockchain.proof_of_work(last_proof))
    # need to figure out running energyusage on linux...

    # code for block mining reward, sender 0 = new block mined
    blockchain.new_transaction(
        sender="0",
        receiver=node_identifier,
        amount=1,
    )

    # add new block to chain
    previous_hash = blockchain.hash(last_block)
    block = blockchain.new_block(proof, previous_hash)

    response = {
        'message': "New block added to chain",
        'index': block['index'],
        'transactions': block['transactions'],
        'proof': block['proof'],
        'previous_hash': block['previous_hash'],
    }
    return jsonify(response), 200


@app.route('/transactions/new', methods=['POST'])
def new_transaction():
    values = request.get_json()

    required = ['sender', 'receiver', 'amount']
    if not all(k in values for k in required):
        return 'Missing values', 400

    index = blockchain.new_transaction(values['sender'], values['receiver'], values['amount'])
    response = {'message': f'Adding transaction to Block {index}'}
    return jsonify(response), 201


@app.route('/chain', methods=['GET'])
def full_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain),
    }

    return jsonify(response), 200


@app.route('/nodes/register', methods=['POST'])
def register_nodes():
    values = request.get_json()

    nodes = values.get('nodes')
    if nodes is None:
        return "Error: Please supply a valid list of nodes", 400

    for node in nodes:
        blockchain.register_node(node)

    response = {
        'message': 'New nodes have been added',
        'total_nodes': list(blockchain.nodes),
    }
    return jsonify(response), 201


@app.route('/nodes/resolve', methods=['GET'])  # function used for consensus between nodes
def consensus():
    replaced = blockchain.resolve_conflict()

    if replaced:
        response = {
            'message': 'chain replaced',
            'new_chain': blockchain.chain
        }
    else:
        response = {
            'message': 'chain is gucci',
            'chain': blockchain.chain
        }

    return jsonify(response), 200


def add_routes():
    app.add_url_rule("/", view_func=views.transaction_page, methods=["Get", "POST"])


if __name__ == '__main__':
    add_routes()
    app.run(host='0.0.0.0', port=5000)