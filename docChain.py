import time
from hashlib import md5


class docChain:
    def __init__(self, blocks):
        self.head = blocks[0]
        self.tail = blocks[len(blocks) - 1]
        self.length = len(blocks)
        self.blocks = blocks
        return None

    def addBlock(self, block):
        self.blocks.append(block)
        self.tail = block
        self.length += 1
        return None

    def addMessage(self, msg):
        msgHash = md5(msg.encode())
        previousHash = md5(self.tail.hashableString().encode()).hexdigest()
        print("adding msg: " + msg)
        nextBlock = block(previousHash, "message", msgHash, msg)
        self.addBlock(nextBlock)
        return None

    def addFile(self, file):

        previousHash = md5(self.tail.hashableString().encode())

        # save the file, set address to next block's docRef
        docRef = ("./docChainFiles/" + file.filename)
        file.save(docRef)

        # get the hash of file's content
        msgHash = md5(open(docRef, 'rb').read()).hexdigest()

        nextBlock = block(previousHash, "file", msgHash, docRef)
        self.blocks.append(nextBlock)
        self.tail = nextBlock
        self.length += 1


def getNewChain():
    genesisBlock = block()
    genesisBlockList = [genesisBlock]
    return docChain(genesisBlockList)


# takes in collection pointer
def getChainFromMDB(db):
    # get all blocks from provided collection
    blocks = db.find({})
    # convert from cursor to list
    blocksList = []
    for block in blocks:
        blocksList.append(block)
    # instantiate doc chain
    chain = docChain(blocksList)
    return chain


class block:
    def __init__(self, previousHash=0, docType=0, docSum=0, docRef=0):
        self.previousHash = previousHash
        self.timestamp = time.time()
        self.docType = docType  # eg: message, binary, .txt, .jpg
        self.docSum = docSum
        self.docRef = docRef  # refers to location of file stored if stored elsewhere
        return None

    def toDict(self):
        return {"prevHash": self.previousHash, "timestamp": self.timestamp, "docType": self.docType,
                "docSum": self.docSum, "docRef": self.docRef}

    def hashableString(self):
        return str(self.previousHash) + str(self.timestamp) + str(self.docType) + str(self.docSum) + str(self.docRef)